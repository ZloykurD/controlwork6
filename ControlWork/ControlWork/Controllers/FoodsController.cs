﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ControlWork.Database;
using ControlWork6.Models;

namespace ControlWork.Controllers
{
  public class FoodsController : Controller
  {
    private readonly ApplicationDbContext _context;

    public FoodsController(ApplicationDbContext context)
    {
      _context = context;
    }

    // GET: Foods
    public async Task<IActionResult> Index()
    {
      var applicationDbContext = _context.Foods.Include(f => f.Restaurant);
      return View(await applicationDbContext.ToListAsync());
    }

    // GET: Foods/Details/5
    public async Task<IActionResult> Details(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var food = await _context.Foods
        .Include(f => f.Restaurant)
        .FirstOrDefaultAsync(m => m.Id == id);
      if (food == null)
      {
        return NotFound();
      }

      return View(food);
    }

    // GET: Foods/Create
    public IActionResult Create()
    {
      ViewBag.RestauranName = _context.Restaurants;
    
      return View();
    }

    // POST: Foods/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Id,Name,Price,Description,RestauranId")]
      Food food)
    {
      if (ModelState.IsValid)
      {
        _context.Add(food);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
      }

      ViewData["RestauranId"] = new SelectList(_context.Restaurants, "Id", "Id", food.RestauranId);
      return View(food);
    }

    // GET: Foods/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var food = await _context.Foods.FindAsync(id);
      if (food == null)
      {
        return NotFound();
      }

      ViewBag.RestauranName = _context.Restaurants;
      return View(food);
    }

    // POST: Foods/Edit/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Price,Description,RestauranId")]
      Food food)
    {
      if (id != food.Id)
      {
        return NotFound();
      }

      if (ModelState.IsValid)
      {
        try
        {
          _context.Update(food);
          await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
          if (!FoodExists(food.Id))
          {
            return NotFound();
          }
          else
          {
            throw;
          }
        }

        return RedirectToAction(nameof(Index));
      }

      ViewData["RestauranId"] = new SelectList(_context.Restaurants, "Id", "Id", food.RestauranId);
      return View(food);
    }

    // GET: Foods/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var food = await _context.Foods
        .Include(f => f.Restaurant)
        .FirstOrDefaultAsync(m => m.Id == id);
      if (food == null)
      {
        return NotFound();
      }

      return View(food);
    }

    // POST: Foods/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
      var food = await _context.Foods.FindAsync(id);
      _context.Foods.Remove(food);
      await _context.SaveChangesAsync();
      return RedirectToAction(nameof(Index));
    }

    private bool FoodExists(int id)
    {
      return _context.Foods.Any(e => e.Id == id);
    }
    public IActionResult Search([FromForm] String name)
    {
      Food model = _context.Foods.FirstOrDefault(r => r.Name.Contains(name));
      
      return View(model);
    }
  }
}