﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ControlWork.Database;
using ControlWork6.Models;

namespace ControlWork.Controllers
{
  public class OrdersController : Controller
  {
    private readonly ApplicationDbContext _context;

    public OrdersController(ApplicationDbContext context)
    {
      _context = context;
    }

    // GET: Orders
    public async Task<IActionResult> Index()
    {
      var applicationDbContext = _context.Orders.Include(o => o.Client).Include(o => o.Food);
      return View(await applicationDbContext.ToListAsync());
    }

    // GET: Orders/Details/5
    public async Task<IActionResult> Details(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var order = await _context.Orders
        .Include(o => o.Client)
        .Include(o => o.Food)
        .FirstOrDefaultAsync(m => m.Id == id);
      if (order == null)
      {
        return NotFound();
      }

      return View(order);
    }

    // GET: Orders/Create
    public IActionResult Create()
    {
      
      ViewBag.ClientName = _context.Clients;
      ViewBag.FoodName = _context.Foods;
      return View();
    }

    // POST: Orders/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Id,Date,FoodId,ClientId")] Order order)
    {
      if (ModelState.IsValid)
      {
        order.Date = DateTime.Now.ToLongDateString();
        _context.Add(order);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
      }

      ViewData["ClientId"] = new SelectList(_context.Clients, "Id", "Id", order.ClientId);
      ViewData["FoodId"] = new SelectList(_context.Foods, "Id", "Id", order.FoodId);
      return View(order);
    }

    // GET: Orders/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var order = await _context.Orders.FindAsync(id);
      if (order == null)
      {
        return NotFound();
      }

      ViewData["ClientId"] = new SelectList(_context.Clients, "Id", "Id", order.ClientId);
      ViewData["FoodId"] = new SelectList(_context.Foods, "Id", "Id", order.FoodId);
      return View(order);
    }

    // POST: Orders/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Id,Date,FoodId,ClientId")] Order order)
    {
      if (id != order.Id)
      {
        return NotFound();
      }

      if (ModelState.IsValid)
      {
        try
        {
          _context.Update(order);
          await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
          if (!OrderExists(order.Id))
          {
            return NotFound();
          }
          else
          {
            throw;
          }
        }

        return RedirectToAction(nameof(Index));
      }

      ViewData["ClientId"] = new SelectList(_context.Clients, "Id", "Id", order.ClientId);
      ViewData["FoodId"] = new SelectList(_context.Foods, "Id", "Id", order.FoodId);
      return View(order);
    }

    // GET: Orders/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var order = await _context.Orders
        .Include(o => o.Client)
        .Include(o => o.Food)
        .FirstOrDefaultAsync(m => m.Id == id);
      if (order == null)
      {
        return NotFound();
      }

      return View(order);
    }

    // POST: Orders/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
      var order = await _context.Orders.FindAsync(id);
      _context.Orders.Remove(order);
      await _context.SaveChangesAsync();
      return RedirectToAction(nameof(Index));
    }

    private bool OrderExists(int id)
    {
      return _context.Orders.Any(e => e.Id == id);
    }
  
  }
}