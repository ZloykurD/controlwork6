﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWork6.Models
{
  public class Order
  {
    public int Id { get; set; }
    public String Date { get; set; }

    public int FoodId { get; set; }
    public Food Food { get; set; }

    public int ClientId { get; set; }
    public Client Client { get; set; }
  }
}