﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWork6.Models
{
  public class Restaurant
  {
    public int Id { get; set; }
    public String Name { get; set; }
    public String Description { get; set; }

    public IEnumerable<Food> Foods { get; set; }
  }
}