﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWork6.Models
{
  public class Client
  {
    public int  Id { get; set; }
    public String Name { get; set; }
    public String Contact { get; set; }

    public IEnumerable<Order> Orders { get; set; }

  }
}
