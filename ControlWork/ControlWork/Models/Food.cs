﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWork6.Models
{
  public class Food
  {
    public int Id { get; set; }
    public String Name { get; set; }
    public double Price { get; set; }
    public String Description { get; set; }

    public int RestauranId { get; set; }
    public Restaurant Restaurant { get; set; }

    public IEnumerable<Order> Orders { get; set; }  
  }
}