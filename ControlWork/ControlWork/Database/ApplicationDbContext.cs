﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork6.Models;
using Microsoft.EntityFrameworkCore;

namespace ControlWork.Database
{
  public class ApplicationDbContext : DbContext
  {
    public DbSet<Client> Clients { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<Restaurant> Restaurants { get; set; }
    public DbSet<Food> Foods { get; set; }


    public ApplicationDbContext(DbContextOptions options) : base(options)
    {
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      //внешний ключ на Food
      modelBuilder.Entity<Order>()
        .HasOne(o => o.Food)
        .WithMany(o => o.Orders)
        .HasForeignKey(o => o.FoodId)
        .OnDelete(DeleteBehavior.Restrict);

      //внешний ключ на Client
      modelBuilder.Entity<Order>()
        .HasOne(o => o.Client)
        .WithMany(o => o.Orders)
        .HasForeignKey(o => o.ClientId)
        .OnDelete(DeleteBehavior.Restrict);

      //внешний ключ на Restaurant
      modelBuilder.Entity<Food>()
        .HasOne(c => c.Restaurant)
        .WithMany(c => c.Foods)
        .HasForeignKey(c => c.RestauranId)
        .OnDelete(DeleteBehavior.Restrict);
    }
  }
}